# from multiprocessing import Pool
import os
import os.path
import Queue
import threading
import time
import datetime
import http_helper
from bs4 import BeautifulSoup
import pickle
__author__ = 'chin'


class ProxyProvider(object):
    singleton = None
    @classmethod
    def instance(cls):
        if not cls.singleton:
            cls.singleton = ProxyProvider()
        return cls.singleton

    def __init__(self):
        self.PICKLE_NAME = 'proxy.pickle'
        self.TESTING_URL = 'http://www.baidu.com'#'http://jsonip.com/'
        self.PROXY_SRC_URL = 'http://51dai.li/http_anonymous.html'
        self._load()
        self.check_all_async()

    def _load(self):
        if os.path.exists(self.PICKLE_NAME):
            p = pickle.load(file(self.PICKLE_NAME))
            self.good_plist = p['good_plist']
            self.all_plist = p['all_plist']
            self.last_check_time = p['last_check_time']
        else:
            self.good_plist = []
            self.all_plist = []
            self.last_check_time = datetime.datetime(2000,1,1)

    def save(self):
        tosave = {'good_plist':self.good_plist,
                  'all_plist':self.all_plist,
                  'last_check_time':self.last_check_time}
        pickle.dump(tosave, file(self.PICKLE_NAME, 'w'))
        print '%d proxy saved. '%(len(self.all_plist),)
        print '%d good proxy saved. '%(len(self.good_plist),)

    def last_check_valid(self):
        return self.last_check_time>datetime.datetime.now() - datetime.timedelta(days=1)

    def check_all_async(self):
        if self.last_check_valid() and len(self.good_plist)>10:
            self.counter = 0
            return
        if len(self.all_plist)<100:
            self.grab_proxies()
        self.last_check_time = datetime.datetime.now()
        self.good_plist = []
        self.counter = -1
        for proxy in self.all_plist:
            while threading.activeCount()>200:
                time.sleep(1)
            t = threading.Thread(target=self.check_proxy_sync, args=(proxy,))
            # t.daemon = True
            t.start()

    def check_proxy_sync(self, proxy):
        try:
            s = http_helper.get_html(self.TESTING_URL, cache=False, proxy=proxy, retry_times=0)
            # print proxy,'returned', s[:20]
            if s[:9]==u'<!DOCTYPE':
                if datetime.datetime.now()-datetime.timedelta(seconds=30)<self.last_check_time:
                    self.good_plist.append(proxy)
                if self.counter==-1:self.counter=0
                print 'add new good proxy, len:', len(self.good_plist)
                return True
        except Exception as e:
            return False

    def next(self):
        while self.counter==-1:
            time.sleep(1)
        pnext = self.good_plist[self.counter%len(self.good_plist)]
        self.counter += 1
        return pnext

    def grab_proxies(self, refresh=True):
        html = http_helper.get_html(self.PROXY_SRC_URL, cache=not refresh)
        self.all_plist = []
        soup = BeautifulSoup(html)
        listtb = soup.find(id='tb')
        ptable = listtb.find_all('table')[0]
        for tr in ptable.find_all('tr')[1:]:
            tds = tr.find_all('td')
            ip = str(tds[1].text)
            port = str(tds[2].text)
            self.all_plist.append((ip, port))
        print len(self.all_plist), 'proxies grabbed'


def main():
    pp = ProxyProvider.instance()
    # for i in range(100):
    #     print pp.next()

    time.sleep(20)
    pp.save()


if __name__ == '__main__':
    main()