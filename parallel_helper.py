import time
import threading
import http_helper
import proxy_helper
import Queue

__author__ = 'chin'

proxies = proxy_helper.ProxyProvider.instance()


def async_get(url, cache=True, use_proxy=True, callback=None ):
    # # how to disable async:
    html = http_helper.get_html(url, cache, use_proxy and proxies.next())
    html = html.replace('\t', ' ')
    if callback:
        callback(html, url)
    return

    def async_func(u, c, p, cb):
        while True:
            try:
                html = http_helper.get_html(u, c, p and proxies.next())
                break
            except Exception as e:
                print 'error:',e,'url:', u, 'try another time with a new proxy'
        if cb:cb(html, url)

    while threading.activeCount() > 200:
        print '.', #'too many threads, sleep main thread for 3 second'
        time.sleep(5)

    t = threading.Thread(target=async_func, args=(url, cache, use_proxy, callback))
    t.start()


def async_multi_get(urls, cache=True, use_proxy=True):
    for idx, url in enumerate(urls):
        proxy = proxies.next() if use_proxy else None
        t = threading.Thread(target=http_helper.get_html, args=(url, cache, proxy))
        t.start()




def main():
    def printfunc(t):
        print t
    url = 'http://jsonip.com'
    async_get(url, cache=False, use_proxy=True, callback=printfunc)
    time.sleep(10)
    return
    urls = ['http://www.douban.com', 'http://www.douban.com', 'http://www.douban.com', 'http://www.douban.com']
    async_multi_get(urls, False)


if __name__ == '__main__':
    main()

