import time
import threading
from bs4 import BeautifulSoup
import json
import http_helper
import proxy_helper
import parallel_helper
import db_helper


proxies = proxy_helper.ProxyProvider.instance()

__author__ = 'chin'


def get_json(url):
    while True:
        try:
            html = http_helper.get_html(url, proxy=proxies.next())
            html = html.replace('\t', ' ')
            # for i,c in enumerate(html):
            #     print i,c
        except Exception as e:
            print 'retry'
            continue
        return json.loads(html)

def get_mainbrands():
    result = []
    r = get_json('http://api.ucar.cn/IphoneApp/UcarApp/carpropertyinfo.ashx?MsgID=MainBrandInfo')
    for by_letter in r['MainBrandDetail']:
        first_letter = by_letter['PinYin']
        for brandinfo in by_letter['GroupBrandInfo']:
            result.append({'id':brandinfo['MainBrandID'], 'name':brandinfo['MainBrandName'],
                           'pic_url':brandinfo['imgURL'], 'first':first_letter})
    # db_helper.bulk_insert('insert into taoche_mainbrand (mbid, name, pic_url, first) values(%s,%s,%s,%s)',
    #                       [(d['id'], d['name'], d['pic_url'], d['first']) for d in result])
    return result


def get_serials():
    mainbrands = get_mainbrands()
    result = []
    for mainbrand in mainbrands:
        url = 'http://api.ucar.cn/IphoneApp/UcarApp/carpropertyinfo.ashx?MsgID=BrandInfo&mainbrandid=%s'%(mainbrand['id'])
        print url
        r = get_json(url)
        brands_group = r['BrandGroup']
        part_result = []
        for g in brands_group:
            brand = g['BrandName']
            serials = g['SerialDetail']
            for serial in serials:
                result.append({'id':serial['SerialID'], 'name':serial['SerialName'], 'pic_url':serial['SerialImgURL'],
                               'brand':brand, 'mainbrandid':mainbrand['id']})
                part_result.append({'id':serial['SerialID'], 'name':serial['SerialName'], 'pic_url':serial['SerialImgURL'],
                               'brand':brand, 'mbid':mainbrand['id']})
        # db_helper.bulk_insert('insert into taoche_serial (sid, name, pic_url, brand, mbid) values (%s,%s,%s,%s,%s)',
        #                   [(d['id'], d['name'], d['pic_url'], d['brand'], d['mbid']) for d in part_result])
    return result

def crawl_trim(s, url):
    print url
    sid = url.split('=')[-1]
    r = json.loads(s)
    trims_by_year = r['YearCarBasic']
    result = []
    for ty in trims_by_year:
        year = ty['YearName']
        for trim in ty['CarBasicDetail']:
            result.append({'id':trim['CarBasicID'], 'name':trim['CarBasicName'], 'year':year, 'sid':sid})
            print result[-1]
    db_helper.bulk_insert('insert into taoche_trim (tid, name, year, sid) values (%s,%s,%s,%s)',
                      [(d['id'], d['name'], d['year'], sid) for d in result])

def get_trims():
    serials = get_serials()
    begin = False
    for serial in serials:
        sid = serial['id']
        if sid=='2217':
            begin = True
        if not begin:
            continue
        url = 'http://api.ucar.cn/IphoneApp/UcarApp/carpropertyinfo.ashx?MsgID=CarBasicInfo&sid=%s'%(sid)
        parallel_helper.async_get(url, callback=crawl_trim)
        # r = get_json(url)

def next_car_list_url(url):
    l, pidx = url.split('pindex=')
    return l+'pindex=%d'%(int(pidx)+1, )


def car_list_cont(html, url):
    r = json.loads(html)
    while True:
        car_list = r['Car_Informations']

        sql = '''insert into taoche_car (
                  city_id, city_name, tid, ucar_id, userid,
                  ucar_s_number, seller_name, car_name, year, price,
                  total_image, transmission, miles, color, phone,
                  image_url, contact, sid, serial_name, exhaust,
                  car_source_cell, ucar_info_url)
   values (%s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s,%s)'''

        data = [(d['CityID'], d['CityName'], d['CarID'], d['UcarID'], d['userid'],
                 d['UcarSNumber'], d['SellerName'], d['CarName'], d['OnTheCarYear'], d['PurchaseMoney'],
                 d['ToTal_Image'], d['Transmission'], d['Mileage'], d['CarColor'], d['Phone'],
                 d['ImageURL'], d['Contact'], d['BrandID'], d['BrandName'], d['Exhaust'],
                 d['CarSource1l'], d['UcarInfoUrl']) for d in car_list]
        # db_helper.bulk_insert(sql, data)

        if len(car_list)>=10:
            url = next_car_list_url(url)
            # print 'new url', url
            r = get_json(url)
        else:
            break



def crawl_car_list():
    sids = db_helper.select_column('select sid from taoche_serial')
    for idx, sid in enumerate(sids):
        print idx, sid
        search_url = 'http://api.ucar.cn/IphoneApp/UcarApp/carListForIphoneApp.ashx?sid=%s&psize=10&pindex=1'%(sid, )
        parallel_helper.async_get(search_url, callback=car_list_cont)


def crawl():
    crawl_car_list()
    # j = get_json('http://api.ucar.cn/IphoneApp/UcarApp/carpropertyinfo.ashx?MsgID=CarBasicInfo&sid=1966')
    # print j
    # get_trims()
    # serials = get_serials()
    # for s in serials:
    #     print s
    # http_helper.DEBUG = True
    # serials = get_serials()
    #
    # for serial in serials[:10]:
    #     name = serial['name']
    #     en_name = serial['urlname'][1:-1]
    #     url = 'http://car.bitauto.com/%s/'%(en_name,)
    #     # print name, en_name, url
    #     db_helper.insert('insert into bitauto_serial (name, en_name, url) values (%s, %s, %s)', (name, en_name, url ))
    #     parallel_helper.async_get(url, callback=crawl_models)


def main():
    crawl()
    return

if __name__ == '__main__':
    main()
