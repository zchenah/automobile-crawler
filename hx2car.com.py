# coding:utf8

import varys
import varys.scheduler
import db_helper
import json

__author__ = 'chin'


varys.scheduler.WORKER_COUNT = 2

def values(dict, keys):
    return [dict[key] for key in keys]


def grab_brands(content, args=None):
    r = json.loads(content)
    brands = []
    for letter_group in r['carSerials']:
        for items in letter_group.values():
            for item in items:
                brands.append(values(item, ('id',  'title', 'letter', 'countryCode', 'levelCode', 'xitupian',
                                            'dirname', 'sellStatus', 'logo', 'brandlist', 'mobileLogo' )))

    # for brand in brands:
    #     print brand[8:10]
    db_helper.bulk_insert('''insert into hx2car_brand (bid, name, first, countryCode, levelCode,
                            xitupian, dirname, sellStatus, logo, brandlist, mobilelogo) values
                            (%s,%s,%s,%s,%s, %s,%s,%s,%s,%s,%s)''', brands)


def grab_cars(content, args):
    page = args
    print page
    # return
    car_lists = json.loads(content)['carList']
    cars = []
    for car in car_lists:
        cars.append(values(car, ['id', 'photoAddress', 'buyDate', 'seriesBrandCarStyle', 'price',
                                 'mileAge', 'credit', 'location', 'publishDate', 'userId'])+[page,])

    db_helper.bulk_insert('''insert into hx2car_car (cid, pic_url, buyDate, name, price,
                mileAge, credit, location, publishDate, userId, page)
    values (%s,%s,%s,%s,%s, %s,%s,%s,%s,%s, %s)''', cars)

def crawl_cars():
    curPage = 1
    for curPage in range(760):
        url = 'http://www.hx2car.com/mobile/search.json?currPage=%d&pageSize=100'%curPage
        varys.add_async(url, callback=grab_cars, args=curPage)


def crawl():
    crawl_cars()
    return
    # url = 'http://www.hx2car.com/mobile/getCarSerials.json'
    # varys.add_async(url, callback=grab_brands)


def main():
    crawl()
    return

if __name__ == '__main__':
    main()

