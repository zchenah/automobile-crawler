# coding:utf8
import time
import threading
from bs4 import BeautifulSoup
import http_helper
import parallel_helper
import db_helper

__author__ = 'chin'


def get_serials():
    soup = http_helper.get_soup('http://car.bitauto.com/brandlist.html')
    # print soup.prettify()
    list_dl = soup(class_='bybrand_list')
    serial_dds = soup('dd', class_='b')
    serials = []
    for serial_dd in serial_dds:
        serial_a = serial_dd.find(class_='brandname').a
        # print serial_a.text, serial_a['href']
        serials.append({'name':serial_a.text, 'urlname':serial_a['href']})
    return serials

def get_province():
    with file('provinces.txt','r') as f:
        return [line.split() for line in f.readlines()]

# def grab_cars_by_shop(html):
#     soup = BeautifulSoup(html)
#     table = soup.find(class_='hotcar_table')
#     if table:
#         h2 = table.find('h2')
#         print h2.a['title']

def grab_shops(src_serial, src_province, pagesoup):
    shops = pagesoup(class_='d_search_list')
    for shop in shops:
        shop_a = shop.h3.a
        name = shop_a.text.strip()
        shopurl = shop_a['href']
        if shopurl.startswith('http://dealer.bitauto.com/'):
            shopid = shopurl.split('/')[-2]
        else:
            shopid = shopurl.split('/')[-2]

        shop_ul = shop.find('ul')
        # print shop_ul
        info_list = shop_ul('li')
        # print info_list
        brands = info_list[0]['title'].strip()
        address = info_list[1]['title'].strip()
        phone_li = info_list[2]
        phone_li.label.extract()
        phone = phone_li.text.strip()

        type_div = shop.find(class_='d_search_class')
        if type_div:
            type = type_div.text.strip()
        else:
            type = ''

        print src_serial, src_province, shopid, name, address, phone, brands, type, shopurl

        db_helper.insert(
            '''insert into bitauto_shop (src_serial, src_province, shopid, name, address, phone, brands, type, url)
             value (%s, %s, %s, %s, %s, %s, %s, %s, %s)'''
                    ,(src_serial, src_province, shopid, name, address, phone, brands, type, shopurl))
        continue
        if shopurl.startswith('http://www.qiche4s.cn/'):
            continue
        carurl = shop_a['href'] + 'cars.html'
        parallel_helper.async_get(carurl, callback=grab_cars_by_shop)




def crawl_all_shops():
    def crawl_shop_list(src_serial, src_province, url):
        soup = http_helper.get_soup(url)
        grab_shops(src_serial, src_province, soup)
        other_pages = soup.find(class_='the_pages')
        if other_pages:
            valid_pages = other_pages('a')
            for page_a in valid_pages[1:-1]:
                new_url = 'http://dealer.bitauto.com'+page_a['href']
                print new_url
                soup = http_helper.get_soup(new_url)
                grab_shops(src_serial, src_province, soup)
                # parallel_helper.async_get(new_url)
    provinces = get_province()
    serials = get_serials()
    for serial in serials:
        for province in provinces:
            url = 'http://dealer.bitauto.com/%s%s'%(province[1], serial['urlname'])
            print province[0], serial['name'], url
            crawl_shop_list(province[0], serial['name'], url)


def trim_detail(html, from_url):
    pass

def trims_by_year(html, from_url):
    soup = BeautifulSoup(html)
    nav_div = soup.find(class_='bt_navigatev1New')
    year = nav_div.strong.text[:-1]
    nav_as = nav_div('a')
    if len(nav_as)==5:
        serialname=nav_as[2].text
        subserial_name=nav_as[3].text
        model_name=nav_as[4].text
    elif len(nav_as)==4:
        serialname=nav_as[2].text
        subserial_name = ''
        model_name = nav_as[3].text
    trim_list = soup.find(id='car_list')
    trim_trs = trim_list('tr')[1:]
    for trim_tr in trim_trs:
        trim_a = trim_tr.a
        if trim_a: #or this is a th row
            trim_name = trim_a.text
            trim_url = trim_a['href']
            trim_id = trim_url.split('/')[-2][1:]
            print 'trim:', serialname, subserial_name, model_name, year, trim_name, trim_url, trim_id
            parallel_helper.async_get(trim_url, callback=trim_detail)
            db_helper.insert(
                '''insert into bitauto_trim (serial_name, subserial_name, model_name,name, year, trim_url, trim_id)
                 values (%s, %s, %s, %s, %s, %s, %s )''',
                    ( serialname, subserial_name, model_name, trim_name,year, trim_url, trim_id))

def crawl_trims(html, from_url):
    soup = BeautifulSoup(html)
    trim_list = soup.find(id='car_list')
    if not trim_list:
        # invalid name: biaozhi206_2985
        print from_url
        return
    head_h3 = trim_list.h3
    years_link = head_h3('a')
    for year_a in years_link:
        by_year_url = 'http://car.bitauto.com'+ year_a['href']
        year = year_a['href'].split('/')[-2]
        if int(year)< 1900:
            continue
        print 'year:', year, by_year_url
        parallel_helper.async_get(by_year_url, callback=trims_by_year)


def crawl_models(html, from_url):
    soup = BeautifulSoup(html)
    serialname = soup.h1.text.strip()
    model_list = soup.find(class_='zppbj')
    model_dls = model_list('dl')
    for model_dl in model_dls:
        pic_dt = model_dl.dt
        en_name = pic_dt.a['href'][1:-1]
        modelurl = 'http://car.bitauto.com/%s/'%(en_name,)

        picurl = pic_dt.img['src']
        info_dd = model_dl.dd
        info_ps = info_dd('p')
        name_a = info_ps[0].a

        model_name = name_a['title'].strip() if name_a.has_attr('title') else name_a.text
        pricerange = info_ps[1].text.strip()
        subserial_name = ''
        print 'model:', serialname, subserial_name, model_name, en_name, picurl, modelurl, pricerange

        db_helper.insert(
            '''insert into bitauto_model (serial_name, subserial_name, name, en_name, pic_url, model_url, price_range)
             values (%s, %s, %s, %s, %s, %s, %s )''', ( serialname, subserial_name, model_name, en_name, picurl, modelurl, pricerange ))
        parallel_helper.async_get(url=modelurl, cache=True, callback=crawl_trims)


def crawl():
    # http_helper.DEBUG = True
    serials = get_serials()

    for serial in serials[:10]:
        name = serial['name']
        en_name = serial['urlname'][1:-1]
        url = 'http://car.bitauto.com/%s/'%(en_name,)
        # print name, en_name, url
        db_helper.insert('insert into bitauto_serial (name, en_name, url) values (%s, %s, %s)', (name, en_name, url ))
        parallel_helper.async_get(url, callback=crawl_models)


def main():
    crawl()
    return

if __name__ == '__main__':
    main()
