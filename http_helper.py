# coding:utf-8
import urllib2
import leveldb
from bs4 import BeautifulSoup
# import requests
import time
import datetime
import json
import re

import os.path
from dateutil.parser import parse

__author__ = 'chin'

HTTP_CACHE_PATH = os.path.join(os.path.dirname(__file__), 'http-cache')

html_cache_ldb = leveldb.LevelDB(HTTP_CACHE_PATH)


CACHE_VALID = datetime.timedelta(days=100)
HTTP_RETRY_LIMIT = 5
HTTP_RETRY_INTERVAL = 2  #senconds
DEBUG = False

#HTTP_TIMEOUT = 10
#urllib2.socket.setdefaulttimeout(HTTP_TIMEOUT)


def debug_print(s):
    if not DEBUG:
        return
    print s


def get_soup(url, cache=True, proxy=None):
    return BeautifulSoup(get_html(url, cache=cache, proxy=proxy))


def get_html(url, cache=True, proxy=None, retry_times=3):
    """proxy is a tuple of str: (ip, port)"""
    cache_key = 'html-'+url
    cached = None
    if cache:
        try:
            cached = json.loads(html_cache_ldb.Get(cache_key))
        except KeyError as e:
            pass
    now = datetime.datetime.now()
    if cached and now - parse(cached['time'])<CACHE_VALID:
        debug_print('cache get: %s'%(url,))
        return cached['html']
    else:
        if proxy:#I don't know how to use proxy in requests
            debug_print('get from proxy: %r'%(proxy, ))
            resp = request_by_proxy(url, proxy)
        else:
            debug_print('get without proxy: %s'%(url))
            resp = request(url)
        if resp.status_code == 200:
            debug_print('http returned %r'%(resp.status_code))
            html_cache_ldb.Put(cache_key, json.dumps({'html':resp.text, 'time': str(now)}))
            return resp.text
        else:
            debug_print('invalid response. error code: %d, url:%s'%(resp.status_code, url))
            retry_times -= 1
            if retry_times<=0:
                raise Exception('reached retry limit!')
            debug_print('sleep and retry time %d'%(retry_times,))
            time.sleep(HTTP_RETRY_INTERVAL)
            return get_html(url, cache, proxy, retry_times)


class FakeResponse(object):
    def __init__(self, status, response_txt):
        self.status_code = status
        self.text = self.unify_encode(response_txt)
    def unify_encode(self, response_txt):
        return unicode(response_txt, 'utf8', errors='ignore')

def request(url):
    # proxies = {"http":"%s:%s"%proxy, "https":"%s:%s"%proxy}
    # proxy = urllib2.ProxyHandler(proxies)
    # opener = urllib2.build_opener(proxy)
    # urllib2.install_opener(opener)
    try:
        r = urllib2.urlopen(url)
    except urllib2.HTTPError as e:
        return FakeResponse(e.code, e.fp.read())
    return FakeResponse(r.getcode(), r.read())

def request_by_proxy(url, proxy):
    proxies = {"http":"%s:%s"%proxy, "https":"%s:%s"%proxy}
    proxy = urllib2.ProxyHandler(proxies)
    opener = urllib2.build_opener(proxy)
    opener.addheaders = [('User-agent', ' Googlebot/2.1 (+http://www.google.com/bot.html)')]
    # urllib2.install_opener(opener)
    try:
        r = opener.open(url)
    except urllib2.HTTPError as e:
        return FakeResponse(e.code, e.fp.read())
    return FakeResponse(r.getcode(), r.read())


def main():
    url = 'http://etao.com'
    url = 'http://jsonip.com'
    # html = get_html(url, cache=False, proxy=None)
    # print html
    #
    # html = get_html(url, cache=True, proxy=None)
    # print html
    #
    # html = get_html(url, cache=False, proxy=('111.64.89.97','8080'))
    html = get_html(url, cache=False, proxy=('203.93.104.20','80'))
    print html


if __name__ == '__main__':
    # try:
    #     r = urllib2.urlopen('http://g.cn/asdf')
    # except urllib2.HTTPError as e:
    #     FakeResponse(e.code, e.fp.read())
    #     # print e.msg
    #     # print e.headers
    #     pass
    # asdf
    main()















