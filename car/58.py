# coding:utf8

import varys
import traceback
import sys

reload(sys)  # Reload does the trick!
sys.setdefaultencoding('UTF8')

from urllib import quote
from collections import defaultdict
import varys.scheduler
import db_helper
import json


from bs4 import BeautifulSoup


__author__ = 'chin'


def build_url(city, type, idx = 1):
    return 'http://%s.58.com/%s/pn%d/'%(city, type, idx)

def grab_cars(content, args):
    city = args[1]
    cars = []
    soup = BeautifulSoup(content)
    cars_table = soup.find('table', class_='tbimg')
    if not cars_table:
        print 'bad data'
        print args
        varys.add_async(args[0], callback=grab_cars, use_cache=False, args=args)
        return
    try:
        for tr in cars_table('tr'):
            tds = tr('td')
            img = tds[0].find('img')
            if not img:
                continue
            img_url = img.get('src') or img['lazy_src']
            desc_td = tds[1]
            desc_a = desc_td.find('a')
            url = desc_a['href']
            descriptions = desc_a.text.split('[')
            time_diff_span = desc_td.find(class_='c_999')
            time_diff = time_diff_span.text if time_diff_span else u''

            # print len(descriptions), desc_td.find('a').text
            trim = descriptions[0]
            intro1 = descriptions[1][:-1] if len(descriptions)>1 else u''
            intro2 = desc_td.find('br').nextSibling.strip() if desc_td.find('br') else u''
            parameter_p = desc_td.find('p')
            parameters = parameter_p.text if parameter_p else u''
            parameters = u'/'.join(p.strip() for p in parameters.split('/'))
            price = tds[2].text

            # print img_url, trim, intro1, intro2, parameters, url, time_diff, price
            cars.append( (img_url, trim, intro1, intro2, parameters, url, time_diff, price, city) )
        db_helper.bulk_insert('''insert into 58_car (img_url, trim, intro1, intro2, parameters, url, time_diff, price, city)
                values (%s, %s, %s, %s, %s, %s, %s, %s, %s)''', cars)
    except Exception as e:
        print cars
        # print soup
        # print cars_table
        print e

        print traceback.format_exc()
        exit_now

    # print args, 'done'
    pass
def crawl_by_city_type(city, type):
    idx = 1
    url = build_url(city, type, idx)
    print url
    r = varys.get_sync(url)
    soup = BeautifulSoup(r.content)
    try:
        total = soup.find(id='infocont')('strong')[0].text
        print total
    except Exception as e:
        print city, type, 'skip'
        return
    page_count = (int(total)+49)/50
    while idx<=page_count:
        url = build_url(city, type, idx)
        varys.add_async(url, grab_cars, args=[url, city, type, idx])
        print idx, 'added'
        idx += 1

def get_city_names():
    url = 'http://www.58.com/changecity.aspx'
    r = varys.get_sync(url)
    soup = BeautifulSoup(r.content)
    city_as = soup.find(id='clist')('a')
    names = []
    for city_a in city_as:
        name = city_a['onclick'].split("'")[1]
        names.append(name)
    return names


def crawl_by_city(city):
    types = ['jingjijiaoche', 'yeuyeche', 'mpv', 'paoche', 'keche', 'huoche', 'mianbaoche', 'pika', 'gongchengche']
    #轿车越野车/SUVMPV跑车客车货车面包车皮卡工程车
    for type in types:
        print city, type, 'begin'
        crawl_by_city_type(city, type)


def crawl():
    cities = get_city_names()
    print cities
    # crawl_by_city('bj')
    for city in cities:
        crawl_by_city(city)
    return


def main():
    crawl()
    return

if __name__ == '__main__':
    main()


