# coding:utf8
import varys
import varys.http
import db_helper
import json
__author__ = 'chin'

varys.http.UA = 'autohome_usedcar client/2.2.1'

def values(dict, keys):
    return [dict[key] for key in keys]

def printfunc(content, args=None):
    r = json.loads(content)
    brands = []
    for brand in r:
        brands.append(values(brand, ('Name','Id','BrandImg', 'FirstLetter')))
        serials = []
        for serial in brand['Series']:
            serial['brand'] = brand['Name']
            serial['bid'] = brand['Id']
            serials.append(values(serial, ['Name', 'Id', 'brand', 'bid', 'FirstLetter']))

        db_helper.bulk_insert('insert into che168_serial (name, sid, brand, bid, first) values (%s,%s,%s,%s,%s)', serials)

    db_helper.bulk_insert('insert into che168_brand (name, bid, pic_url, first) values (%s,%s,%s,%s)', brands)

def cars_cb(content, args=None):
    print args
    r = json.loads(content)
    items = r['result']['items']
    cars = []
    for item in items:
        cars.append(values(item, (  'id', 'name', 'completesale', 'price', 'image', 'mileage', 'registrationdate',
                                    'source', 'publishdate', 'hascard', ) ))

    db_helper.bulk_insert('''insert into che168_car (
    cid, name, completesale, price, image, mileage, registrationdate, source, publishdate, hascard) values (%s,%s,%s,
    %s,%s,%s, %s,%s,%s, %s)''', cars)

def crawl_cars():
    rawurl = 'http://sp.autohome.com.cn/2sc/quotesearch.ashx?pageSize=100&pageIndex='
    for pidx in range(1, 570):
        url = rawurl+str(pidx)
        varys.add_async(url, callback=cars_cb, args=pidx)



def crawl():
    crawl_cars()
    return
    url = 'http://sp.autohome.com.cn/2sc/seriesAll.ashx'
    varys.add_async(url, printfunc)


def main():
    crawl()
    return

if __name__ == '__main__':
    main()

