# coding:utf8

import varys
from urllib import quote
from collections import defaultdict
import varys.http
import db_helper
import json
import re

from bs4 import BeautifulSoup


varys.http.DEFAULT_ENCODING = 'gbk'

__author__ = 'chin'

def grab_trims(content, args):
    sid = args
    soup = BeautifulSoup(content)
    cur = soup.find('dt')
    year = ''
    trims = []
    while cur!=None:
        if cur.name=='dt':
            year = cur.text
        if cur.name=='dd':
            oc = cur.a['onclick']
            re_g = re.search("Click\((.+)\,\'(.+)\'", oc)
            tid, tname = re_g.group(1), re_g.group(2)
            trims.append((tid, tname, year, sid))
        cur = cur.next_sibling
    db_helper.bulk_insert('''insert into che168_trim (tid, name, year, sid)
            values (%s, %s, %s, %s)''', trims)



def crawl_trims(sid):
    url = 'http://www.che168.com/UserCenter/handler/SaleCar/GetCarCategory.ashx?type=3&seriesid=%s'%(sid,)
    print url
    varys.add_async(url, callback=grab_trims, args=sid)


def main():
    sids = db_helper.select_column('select sid from che168_serial')
    for sid in sids:
        crawl_trims(sid)

    return


if __name__ == '__main__':
    main()