# coding:utf8

import varys
from urllib import quote
from collections import defaultdict
import varys.scheduler
import db_helper
import json

from bs4 import BeautifulSoup


__author__ = 'chin'


def grab_trims(content, args):
    sname, sid,url = args
    # 58 puts serial info in the trim interface.. BREAK THE FORM!
    serial_info_items = json.loads(content)
    # if len(serial_info_items)!=4:
    #     print sname, sid, url
    serial_type, serial_trims, serial_madein, serial_alias = None, None, None, None
    for si_item in serial_info_items:
        if si_item['name']==u'类型':
            serial_type = si_item
        elif si_item['name']==u'车系别名':
            serial_alias = si_item
        elif si_item['name']==u'产地':
            serial_madein = si_item
        elif si_item['name']==u'车型':
            serial_trims = si_item
        else:
            print si_item['name'], 'unknown!!!'
    stype_id, stype_name = (serial_type['value'][0]['v'], serial_type['value'][0]['t']) if serial_type else ('','')
    smadein_id, smadein_name = (serial_madein['value'][0]['v'], serial_madein['value'][0]['t']) if serial_madein else ('','')
    salias_id, salias_name = (serial_alias['value'][0]['v'], serial_alias['value'][0]['t']) if serial_alias else ('','')
    trims = []
    if serial_trims:
        for trim_item in serial_trims['value']:
            tid, tname, ti = trim_item['v'], trim_item['t'], trim_item['i']
            trims.append((tid, tname, ti, sid, sname, stype_id, stype_name, smadein_id, smadein_name, salias_id, salias_name))
            # print tid, tname, ti, sid, sname, stype_id, stype_name, smadein_id, smadein_name, salias_id, salias_name
    else:
        trims = [('-1', '-1', '-1', sid, sname, stype_id, stype_name, smadein_id, smadein_name, salias_id, salias_name)]
    db_helper.bulk_insert('''insert into 58_trim (tid, name, ti, sid, sname,
                stype_id, stype_name, smadein_id, smadein_name, salias_id, salias_name)
                values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)''', trims)

def crawl_trims(sname, sid):
    url = 'http://post.58.com/ajax/?action=getchildrenpara&cateid=29&nameid=5867&vid=%s&key=%s'%(sid, quote(sname.encode('utf8')))
    varys.add_async(url, callback=grab_trims, args = (sname, sid, url))


def grab_serials(content, args=None):
    brandname = args
    serial_tuples = json.loads(content)[0]['value']
    serials = []
    for s in serial_tuples:
        si, sid, sname = s['i'], s['v'], s['t']
        serials.append((sid, sname, si, brandname))
        crawl_trims(sname, sid)
    # db_helper.bulk_insert('''insert into 58_serial (sid, name, si, bname)
    #         values (%s, %s, %s, %s)''', serials)

def get_serials(brandname):
    print brandname
    url = 'http://post.58.com/ajax/?action=getchildrenpara&cateid=29&cityid=1&nameid=5866&vid=335537&key=%s'%(quote(brandname.encode('utf8')))
    varys.add_async(url, callback=grab_serials, args = brandname)

def crawl():
    root_url = 'http://post.58.com/1/29/s5'
    r = varys.get_sync(root_url)
    soup = BeautifulSoup(r)
    lists = soup(class_='fenleilist')
    for l in lists:
        brand_dds = l('dd')
        print l['zm'] # first OR class of the brand
        if len(l['zm'])==1:
            brands = []
            for brand_dd in brand_dds:
                if brand_dd.has_attr('zm'):
                    first, brandname = brand_dd['zm'], brand_dd.text
                    brands.append((brandname, first, ))
                    get_serials(brandname)
            # db_helper.bulk_insert(
            #     'insert into 58_brand (name, first) values (%s, %s)', brands)
    return


def main():
    crawl()
    return

if __name__ == '__main__':
    main()


