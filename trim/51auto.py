# coding:utf8

import varys
from urllib import quote
from collections import defaultdict
import varys.scheduler
import varys.http
import varys.util
import db_helper
import json

import base64

from bs4 import BeautifulSoup


__author__ = 'chin'


def get51(info_dict):
    info_json = json.dumps(info_dict)
    # print info_json
    info_b64 = base64.b64encode(info_json)
    # print info_b64
    url = 'http://w.51auto.com:6666/2car/v1.3/androidService/androidServlet?info=%s'%(info_b64)
    resp = varys.http.get(url, use_cache=True)
    # content = varys.util.try_best_to_get(url)
    r = base64.b64decode(resp.content).decode('gbk')

    print r
    return json.loads(r)

def get_brands():
    info_dict = {"body":{"id":"1774455"},"header":{"dataSize":0,"messageId":0,"model":0,"service":3006}}
    r = get51(info_dict)
    brand_items = r['body']
    brands = []
    for b in brand_items:
        brands.append([b['brand'], b['key'], b['imgUrl']])

    # db_helper.bulk_insert(
    #     'insert into 51auto_brand (name, first, pic_url) values (%s, %s, %s)', brands)
    return brands

def get_serials(brandname):
    print brandname
    info_dict = {"body":{"brand":brandname},"header":{"dataSize":0,"messageId":0,"model":0,"service":3007}}
    r = get51(info_dict)
    serial_items = r['body']
    serials = []
    for s in serial_items:
        serials.append((s['family'], s['makecode'], s['img'], s['makeDesc'], s['vehicle'], s['desc'], brandname))

    # db_helper.bulk_insert('''insert into 51auto_serial (name, makecode, pic_url, makedesc, vehicle, f_desc, bname)
    #  values (%s, %s, %s, %s, %s, %s, %s)''', serials)
    return serials




def crawl_trims(family, makecode, sname):
    print family, makecode, sname
    info_dict = {"body":{"family":family, "makecode":makecode},"header":{"dataSize":0,"messageId":0,"model":0,"service":3008}}
    r = get51(info_dict)
    years = r['body']
    trims = []
    for by_year in years:
        year_desc = by_year['desc']
        year_num = by_year['vehicleYear']
        for t in by_year['carFileList']:
            tname, tid = t['desc'], t['vehicleKey']
            trims.append((tname, tid, year_desc, year_num, family, makecode, sname))

    db_helper.bulk_insert('''insert into 51auto_trim (name, tid, year_desc, year_num, family, makecode, sname)
                values (%s, %s, %s, %s, %s, %s, %s)''', trims)


def crawl():
    brands = get_brands()
    for brand,key,img in brands:
        serials = get_serials(brand)
        for s in serials:
            crawl_trims(s[0], s[1], s[4])

def test():
    info_dict = {"body":{"brand":"奥迪"},"header":{"dataSize":0,"messageId":0,"model":0,"service":3007}}
    r = get51(info_dict)
    print r
    brand_items = r['body']

    pass

def main():
    # test()
    # return
    crawl()
    print 'main return'
    return

if __name__ == '__main__':
    main()


