# coding:utf8

import varys
from urllib import quote
from collections import defaultdict
import varys.scheduler
import db_helper
import json

from bs4 import BeautifulSoup


__author__ = 'chin'


def grab_trims(content, args):
    sname, sid = args
    trims = []
    for trim_tuple in json.loads(content):
        tid, tname, tbool, tname2 =  trim_tuple
        if int(tid)==0:
            continue
        print tid,'|', tbool,'|',  tname, tname2
        trims.append((tname, tid, tname2,tbool,  sid, sname ))
    db_helper.bulk_insert('''insert into ganji_trim (name, tid, name2, tbool, sid, sname)
            values (%s, %s, %s, %s, %s, %s)''', trims)


def crawl_trims(sname, sid):
    url = 'http://www.ganji.com/ajax.php?module=get_che_xing'
    postdata = 'tagId=%s&with_all_option=1'%(sid)
    varys.add_async(url, callback=grab_trims, data=postdata, args = (sname, sid))

def grab_serials(content, args=None):
    # print args, content
    bid, bname = args
    serials = []
    serial_tuples = json.loads(content)
    for s in serial_tuples:
        sid, spinyin, sbool,sname = s
        if int(sid)==-1:
            continue
        # print sid, sname, sbool,spinyin
        serials.append((sid, sname, spinyin, sbool, bid, bname ))
        crawl_trims(sname, sid)
        pass
    # db_helper.bulk_insert('''insert into ganji_serial (sid, name, pinyin, sbool, bid, bname)
    #         values (%s, %s, %s, %s, %s, %s)''', serials)

def crawl_serials(bid, bpinyin, bname):
    # print bid, bpinyin, bname
    postdata = 'minorCategoryId=%s&with_all_option=1'%(bid,)
    url = 'http://www.ganji.com/ajax.php?module=get_tag'
    varys.add_async(url, callback=grab_serials, data=postdata, args=(bid, bname))
    # grab_serials(content=varys)
    # print varys.get_sync(url)


def crawl():
    root_url = 'http://www.ganji.com/pub/pub.php?act=pub&method=load&cid=6&mcid=14&domain=bj&domain=bj'
    r = varys.get_sync(root_url)
    soup = BeautifulSoup(r)
    brand_div = soup(id='allBrand')[0]
    letter_spans = brand_div('span')[1:]
    # brands = []
    for letter_span in letter_spans:
        first = letter_span['id'].split('-')[-1]
        for brand_a in letter_span('a'):
            bid, bpinyin, bname = brand_a['_v'].split(',')
            # brands.append((bid, bpinyin, bname, first))
            crawl_serials(bid, bpinyin, bname)
    # db_helper.bulk_insert('insert into ganji_brand (bid, pinyin, name, first) values (%s, %s, %s, %s)', brands)
    return


def main():
    crawl()
    return

if __name__ == '__main__':
    main()


