# coding:utf8

import varys
from urllib import quote
from collections import defaultdict
import varys.http
import db_helper
import json

from bs4 import BeautifulSoup

__author__ = 'chin'


varys.http.DEFAULT_ENCODING = 'gbk'

def grab_serials(content, args=None):
    # print args, content
    bid, bname = args
    serials = []
    serial_tuples = json.loads(content)
    for s in serial_tuples:
        sid, spinyin, sbool,sname = s
        if int(sid)==-1:
            continue
        # print sid, sname, sbool,spinyin
        serials.append((sid, sname, spinyin, sbool, bid, bname ))
        crawl_trims(sname, sid)
        pass


def grab_brands(line):
    brandsstr = line.split('=')[-1][1:-3]
    brand_tuples = zip(*(iter(brandsstr.split(',')),) * 2)
    brands = []
    for b in brand_tuples:
        bid = b[0]
        first, bname = b[1].split(' ')
        print bid, first, bname
        brands.append(( bid, first, bname))
    # db_helper.bulk_insert('insert into xcar_brand (bid, first, name ) values (%s, %s, %s)', brands)


def crawl():
    root_url = 'http://used.xcar.com.cn/post.htm'
    super_js_url = 'http://icon.xcar.com.cn/pub_js/used_arr_pb_ps_m.js'
    resp = varys.http.get(url = super_js_url, use_cache=True)
    content = resp.content
    lines = content.splitlines()
    ps_arr, m_arr = {},{}
    for l in lines:
        if l.startswith('var pb_arr='):
            grab_brands(l)
        elif l.startswith('ps_arr['): #serial info
            exec(l, globals(), locals())
        elif l.startswith('m_arr['): #trim_info
            exec(l, globals(), locals())
        else:
            pass
    for sid, trims_str in m_arr.items():
        print sid
        trim_tuples = zip(*(iter(trims_str[2:].split(',')),) * 2)
        trims = []
        for t in trim_tuples:
            tid, tname = t
            trims.append((tid, tname, sid))
        db_helper.bulk_insert('''insert into xcar_trim (tid, name, sid)
            values (%s, %s, %s)''', trims)

    return
    for bid, serials_str in ps_arr.items():
        print bid
        serial_tuples = zip(*(iter(serials_str[2:].split(',')),) * 2)
        serials = []
        for s in serial_tuples:
            sid, sname = s
            serials.append((sid, sname, bid))
        # db_helper.bulk_insert('''insert into xcar_serial (sid, name, bid)
        #     values (%s, %s, %s)''', serials)
    return

def main():

    crawl()
    return


if __name__ == '__main__':
    main()