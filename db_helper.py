# coding:utf8
import MySQLdb
import threading


__author__ = 'chin'



db = MySQLdb.connect(host='127.0.0.1',
                     user='root',
                     passwd="123456",
                     db="crawler_raw",
                     port=3306,
                     charset='utf8',
                     use_unicode=True)

db.set_character_set('utf8')
dbc = db.cursor()
dbc.execute('SET NAMES utf8;')
dbc.execute('SET CHARACTER SET utf8;')
dbc.execute('SET character_set_connection=utf8;')
db.commit()

cursor = db.cursor()
cursor.execute("SELECT VERSION()")

ver = cursor.fetchone()

print "Database version : %s " % ver

def mig_table(tablename):
    execute('DROP TABLE IF EXISTS car_meta.%s;' %(tablename,))
    execute('CREATE TABLE car_meta.%s SELECT * FROM crawler_raw.%s;'%(tablename, tablename))

def count_table(tn):
    sql = 'select count(*) from car_meta.%s'%(tn)
    count = select_scalar(sql)
    return count

def trims():
    sites = ['58', 'ganji', '51auto', 'che168', 'taoche', 'xcar']
    tables = ['brand', 'serial', 'trim']
    for s in sites:
        print s,
        for t in tables:
            tablename = '%s_%s'%(s,t)
            c = count_table(tablename)
            print c,
        print

def execute(sql):
    cursor = db.cursor()
    cursor.execute(sql)
    db.commit()

def insert(sql, data_list):
    cursor = db.cursor()
    cursor.execute(sql, data_list)
    db.commit()

db_lock = threading.Lock()
def bulk_insert(sql, data_list_list):

    db_lock.acquire()

    cursor = db.cursor()
    cursor.executemany(sql, data_list_list)
    db.commit()

    db_lock.release()


def select(sql):
    cursor = db.cursor()
    cursor.execute(sql)
    result = cursor.fetchall()
    return result

def select_column(sql):
    result = select(sql)
    return [row[0] for row in result]

def select_row(sql):
    result = select(sql)
    return result[0]

def select_scalar(sql):
    row = select_row(sql)
    return row[0]


def main():
    trims()
    print 'main return'
    return

if __name__ == '__main__':
    main()


